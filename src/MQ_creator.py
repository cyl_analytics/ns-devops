#!/usr/bin/python

from __future__ import print_function

import os
import string
import random
import requests

METADATA_URL = "http://metadata.google.internal/computeMetadata/v1/"
METADATA_HEADERS = {'Metadata-Flavor': 'Google'}
NAUTICAL_URL = "10.240.0.9"
NAUTICAL_PORT = "8080"


def get_internalip():
    """
    return internal ip
    """
    return requests.get(METADATA_URL + "instance/network-interfaces/0/ip",
                        headers=METADATA_HEADERS).text()


def get_id():
    """
    return machine identificator
    """
    return requests.get(METADATA_URL + "instance/id",
                        headers=METADATA_HEADERS).text()


def get_random_string():
    """
    Generate random string
    """
    return ''.join(random.choice(string.ascii_uppercase + string.digits)
                   for _ in range(42))


def main():
    os.system(
        'echo "deb http://binaries.erlang-solutions.com/debian precise contrib" | sudo tee /etc/apt/sources.list.d/esl.list > /dev/null')
    os.system(
        'wget -O - http://binaries.erlang-solutions.com/debian/erlang_solutions.asc | apt-key add -')
    os.system('apt-get update')
    os.system('apt-get install -y esl-erlang')
    os.system(
        'echo "deb http://www.rabbitmq.com/debian/ testing main"  | tee  /etc/apt/sources.list.d/rabbitmq.list > /dev/null')
    os.system('wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -')
    os.system('apt-get update')
    os.system('apt-get install rabbitmq-server -y')
    os.system('service rabbitmq-server start')
    os.system('rabbitmq-plugins enable rabbitmq_management')
    os.system('service rabbitmq-server restart')

    mq_user = get_random_string()[:10]
    mq_password = get_random_string()

    os.system('rabbitmqctl add_user {} {}'.format(mq_user, mq_password))
    os.system('rabbitmqctl set_permissions {} ".*" ".*" ".*"'.format(mq_user))

    r = requests.post("{}:{}/api/v1/metadata".format(NAUTICAL_URL, NAUTICAL_PORT),
                  auth=(get_id(), get_id()),
                  data={"mq_host": get_internalip(),
                        "mq_port": 5672,
                        "mq_login": get_random_string(),
                        "mq_password": get_random_string()})

    if r.status_code != 200:
        exit(1)

if __name__ == "__main__":
    main()
