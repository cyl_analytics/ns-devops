#!/usr/bin/env python
#-*- coding: utf-8 -*-

from __future__ import print_function

import os
import signal
import hashlib
import argparse
import requests

METADATA_URL = "http://metadata.google.internal/computeMetadata/v1/"
METADATA_HEADERS = {'Metadata-Flavor': 'Google'}
PROXY_DIRECTORY = '/cloudsql'
INSTANCE_CONNECTION_NAME = 'ns-analytics:europe-west1:ns-analytics-db'

KEY_FILE_NAME = "/var/opt/cintru.webapi.key"
CERT_FILE_NAME = "/var/opt/cintru.webapi.cert"

PATH_TO_USER_COMMANDS = "/usr/bin/"
CLOUD_SQL_PROXY_BIN = os.path.join(PATH_TO_USER_COMMANDS, "cloud_sql_proxy")
EXE_BIN = os.path.join(PATH_TO_USER_COMMANDS, "webapi-exe")


def hashfile(afile, hasher, blocksize=65536):
    """
    Calculate hash sum of file.
    Get from here <http://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file>

    Args:
      afile :: File -- open file (file descriptor)
      hasher :: Function  -- hash function
      blocksize :: Int
    Return:
      Int
    """
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.digest()


def get_ip():
    url = METADATA_URL + "instance/network-interfaces/0/access-configs/0/external-ip"
    r = requests.get(url, headers=METADATA_HEADERS)

    if r.status_code != 200:
        print("{} get wrong status code: {}".format(url, r.status_code))
    return r.text.strip()


def systemd_service_config(pre, cmd):
    """
    Here created systemd service config file

    Return:
      String
    """
    return """
[Unit]
Description=cintru reports API

[Service]
Type=simple
ExecStartPre={}
ExecStart={}
Restart=always
    """.format(pre, cmd)

def upstart_service_config(pre, cmd):
    """
    Here created upstart service config file

    Return:
      String
    """
    return """
description "cintru reports web api"
author "dpetrov@cyl.me"

start on runlevel [2345]

script
    {}
end script
pre-start script
    {}
end script
""".format(cmd, pre)



def main(login, pwd):
    """
    Script will work only on the 64-bit linux.
    """
    # Install the MySQL client from the package manager:
    os.system('apt-get update')
    os.system('apt-get install -y mysql-client')

    # Download the proxy
    os.system(
        'wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O {}'.format(
            CLOUD_SQL_PROXY_BIN))
    # Make the proxy executable
    os.system('chmod +x {}'.format(CLOUD_SQL_PROXY_BIN))
    # Create the directory where the proxy sockets will live
    os.system('mkdir {}; chmod 777 {}'.format(PROXY_DIRECTORY,
                                              PROXY_DIRECTORY))
    # Start the proxy
    os.system('{} -dir={} -instances={} &'.format(
        CLOUD_SQL_PROXY_BIN, PROXY_DIRECTORY, INSTANCE_CONNECTION_NAME))
    # Install dependencies
    os.system('apt-get install -y libmysqlclient-dev pkg-config libpcre3-dev')

    os.system('apt-get install -y git')

    # Copy reports sql scripts
    os.system(
        'git clone -q https://{}:{}@github.com/NewSquareRU/reports.git /var/opt/'.format(login, pwd))

    # Copy cintru web server binary from repository
    os.system('gsutil cp gs://ns-repository/ubuntu/14.04/webapi/webapi-exe .')
    os.system("mv webapi-exe {}".format(EXE_BIN))
    os.system('chmod +x {}'.format(EXE_BIN))

    hostip = get_ip()
    # Create self-signed certificate
    os.system(
        'openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=RU/ST=Moscow/L=Moscow/O=NewSquare/CN={}" -keyout {} -out {}'.format(
            hostip, KEY_FILE_NAME, CERT_FILE_NAME))

    # Deploy certificate to storage
    os.system('gsutil cp {} gs://ns-persistent/certificates/'.format(
        CERT_FILE_NAME))

    os.system('touch /var/log/cintru_webapi.log')

    with open("/etc/init/webapi.conf", 'w') as sink:
        sink.write(upstart_service_config(
            'mysql -u root -S {} < /var/opt/reports.sql'.format(
                os.path.join(PROXY_DIRECTORY, INSTANCE_CONNECTION_NAME)),
            '{} --dbPath {} --certFile {} --keyFile {} --port 443 >> /var/log/cintru_webapi.log'.format(
                EXE_BIN, os.path.join(
                    PROXY_DIRECTORY, INSTANCE_CONNECTION_NAME), CERT_FILE_NAME,
                KEY_FILE_NAME)))

    # Run server
    os.system("service webapi start")


def update(login, pwd):
    """
    Update sql reports and download server binary. Check and if it is new kill previos server process and run new one.
    """
    if not os.path.isfile(EXE_BIN):
        print("there is no webapi file. can't update.")
        exit(1)

    # Copy reports sql scripts
    os.system('cd /var/opt')
    os.system('git pull -q https://{}:{}@github.com/NewSquareRU/reports.git'.format(login, pwd))

    prev_webapi_checksum = hashfile(open(EXE_BIN, "rb"), hashlib.sha256())
    # Copy cintru web server binary from repository
    os.system('gsutil cp gs://ns-repository/ubuntu/14.04/webapi/webapi-exe .')
    os.system("mv webapi-exe {}".format(EXE_BIN))
    os.system('chmod +x {}'.format(EXE_BIN))

    current_webapi_checksum = hashfile(open(EXE_BIN, "rb"), hashlib.sha256())

    if current_webapi_checksum == prev_webapi_checksum:
        print("server binary didn't change from last time")
    else:
        os.system("service webapi restart")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--update",
                        help="update existed server",
                        action="store_true")
    parser.add_argument("--githublogin", help="github login")
    parser.add_argument("--githubpwd", help="github password")
    args = parser.parse_args()

    if args.update:
        update(args.githublogin, args.githubpwd)
    else:
        main(args.githublogin, args.githubpwd)
