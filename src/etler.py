#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import datetime

PROXY_DIRECTORY = '/cloudsql'
INSTANCE_CONNECTION_NAME = 'ns-analytics:europe-west1:ns-analytics-db'


def main():
    """
    Script will work only on the 64-bit linux.
    """
    # Install the MySQL client from the package manager:
    os.system('sudo apt-get update')
    os.system('sudo apt-get install -y mysql-client')

    # Download the proxy
    os.system(
        'wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64')
    # Rename the proxy to use the standard filename:
    os.system('mv cloud_sql_proxy.linux.amd64 cloud_sql_proxy')
    # Make the proxy executable
    os.system('chmod +x cloud_sql_proxy')
    # Create the directory where the proxy sockets will live
    os.system('sudo mkdir {}; sudo chmod 777 {}'.format(PROXY_DIRECTORY,
                                                        PROXY_DIRECTORY))
    # Start the proxy
    os.system('sudo ./cloud_sql_proxy -dir={} -instances={} &'.format(
        PROXY_DIRECTORY, INSTANCE_CONNECTION_NAME))
    # Install dependencies
    os.system(
        'sudo apt-get install -y libmysqlclient-dev pkg-config libpcre3-dev git')

    # Initialize db tables
    os.system(
        'git clone https://dpetrovmabius@bitbucket.org/cyl_analytics/ns-devops.git')
    os.system('mysql -u root -S {} < ns-devops/src/db_analytics.sql'.format(
        os.path.join(PROXY_DIRECTORY, INSTANCE_CONNECTION_NAME)))

    # Copy etler binary from repository
    os.system('gsutil cp gs://ns-repository/ubuntu/14.04/etler/etler-exe .')
    os.system('chmod +x etler-exe')

    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    # Run etler
    os.system('./etler-exe --dbPath {} --year {} --month {} --day {} --all'.format(
        os.path.join(PROXY_DIRECTORY, INSTANCE_CONNECTION_NAME),
        yesterday.year, yesterday.month, yesterday.day))
    os.system('sudo shutdown -h now')


if __name__ == "__main__":
    main()
