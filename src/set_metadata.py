#!/usr/bin/env python
#-*- coding: utf-8 -*-

from __future__ import print_function

import argparse
import requests
from apiclient.discovery import build
from oauth2client.client import GoogleCredentials

METADATA_URL = "http://metadata.google.internal/computeMetadata/v1/"
METADATA_HEADERS = {'Metadata-Flavor': 'Google'}


def get_project():
    url = METADATA_URL + "project/project-id"
    r = requests.get(url, headers=METADATA_HEADERS)

    if r.status_code != 200:
        print("{} get wrong status code: {}".format(url, r.status_code))
        return get_project()

    return r.text


def get_zone():
    url = METADATA_URL + "instance/zone"
    r = requests.get(url, headers=METADATA_HEADERS)

    if r.status_code != 200:
        print("{} get wrong status code: {}".format(url, r.status_code))
        return get_zone()

    return last(r.text.split('/'))


def get_instance():
    url = METADATA_URL + "instance/hostname"
    r = requests.get(url, headers=METADATA_HEADERS)

    if r.status_code != 200:
        print("{} get wrong status code: {}".format(url, r.status_code))
        return get_instance()

    return head(r.text.split('.'))


# --- utils ---
def last(l):
    if l and isinstance(l, list):
        return l[-1]
    else:
        return ""


def head(l):
    if l and isinstance(l, list):
        return l[0]
    else:
        return ""


def create_compute_client():
    credentials = GoogleCredentials.get_application_default()
    return build("compute", "v1", credentials=credentials)


def get_fingerprint(md):
    return bytes(md["tags"]["fingerprint"])


def get_externalip(md):
    for n in md["networkInterfaces"]:
        for c in n["accessConfigs"]:
            if c["name"] == "external-nat":
                return c["natIP"]


def set_metadata(items):
    project = get_project()
    zone = get_zone()
    instance = get_instance()

    client = create_compute_client()
    json_request = client.instances().get(project=project,
                                          zone=zone,
                                          instance=instance).execute()
    if "ext_ip_name" in items:
        ext_ip_key = items["ext_ip_name"]
        items.pop("ext_ip_name")
        items[ext_ip_key] = get_externalip(json_request)

    body = dict([("kind", "compute#metadata"), ("fingerprint", get_fingerprint(
        json_request)), ("items", [{"key": k,
                                    "value": v} for k, v in items.items()])])
    return client.instances().setMetadata(project=project,
                                          zone=zone,
                                          instance=instance,
                                          body=body).execute()


def main():
    args = argparse.ArgumentParser()
    args.add_argument("--kv",
                      action="append",
                      type=lambda kv: kv.split('='),
                      dest="kv",
                      help="key value pairs which you want add to metadata")
    print(set_metadata(dict(args.parse_args().kv)))


if __name__ == "__main__":
    main()
