CREATE DATABASE IF NOT EXISTS etlerdbv1;

USE etlerdbv1;

CREATE TABLE IF NOT EXISTS oos(userId INT,  timestamp DATE,taskId INT, nmgkUnit INT, regionId INT, cityId INT, pointId INT, nmgkTtType VARCHAR(10), retailerId INT, itemId INT, categoryId INT, value INT);

CREATE TABLE IF NOT EXISTS shelfshare(userId INT,  timestamp DATE, taskId INT, nmgkUnit INT, regionId INT, cityId INT, pointId INT, nmgkTtType VARCHAR(10), retailerId INT, manufacturerId INT, itemId INT, categoryId INT, value INT);

CREATE TABLE IF NOT EXISTS abphoto (userId INT, timestamp DATE, taskId INT, nmgkUnit INT, regionId INT, cityId INT, pointId INT, nmgkTtType VARCHAR(10), retailerId INT, type VARCHAR(15), value TEXT);

CREATE TABLE IF NOT EXISTS pricemarketing (userId INT, timestamp DATE, taskId INT, nmgkUnit INT, regionId INT, cityId INT, pointId INT, nmgkTtType VARCHAR(10), retailerId INT, skuId INT, skuBrandId INT, skuCategoryId INT, skuManufacturerId INT, price DOUBLE);

CREATE TABLE IF NOT EXISTS trademarketing (userId INT, timestamp DATE, taskId INT, nmgkUnit INT, regionId INT, cityId INT, pointId INT, nmgkTtType VARCHAR(10), retailerId INT, skuId INT, skuBrandId INT, skuCategoryId INT, skuManufacturerId INT, price DOUBLE, discount INT, campaign INT, priceBefore DOUBLE);

CREATE TABLE IF NOT EXISTS newcategories (userId INT, timestamp DATE, taskId INT, nmgkUnit INT, regionId INT, cityId INT, pointId INT, nmgkTtType VARCHAR(10), retailerId INT, skuBrandId INT, skuCategoryId INT, skuManufacturerId INT, price DOUBLE, barCode VARCHAR(30), levelShelves INT, numberOfFaces INT, layoutImages TEXT, images TEXT);

DELIMITER $$

DROP PROCEDURE IF EXISTS csi_add_index $$
CREATE PROCEDURE csi_add_index(in theIndexName varchar(128), in theTable varchar(128), in theIndexColumns varchar(128)  )
BEGIN
  IF ((SELECT COUNT(*) AS index_exists FROM information_schema.statistics WHERE TABLE_SCHEMA = DATABASE() and table_name = theTable AND index_name = theIndexName)  = 0) THEN
    SET @s = CONCAT('CREATE INDEX ' , theIndexName , ' ON ' , theTable, '(', theIndexColumns, ')');
    PREPARE stmt FROM @s;
    EXECUTE stmt;
  END IF;
END $$

DELIMITER ;

CALL csi_add_index("idx_oos_nmgkUnit_timestamp", "oos",  "nmgkUnit, timestamp");
CALL csi_add_index("idx_shelfshare_nmgkUnit_timestamp", "shelfshare", "nmgkUnit, timestamp");
CALL csi_add_index("idx_abphoto_nmgkUnit_timestamp", "abphoto", "nmgkUnit, timestamp");
CALL csi_add_index("idx_pricemarketing_nmgkUnit_timestamp", "pricemarketing", "nmgkUnit, timestamp");
CALL csi_add_index("idx_trademarketing_nmgkUnit_timestamp", "trademarketing", "nmgkUnit, timestamp");
CALL csi_add_index("idx_newcategories_nmgkUnit_timestamp", "newcategories", "nmgkUnit, timestamp");
